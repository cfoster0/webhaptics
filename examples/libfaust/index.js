/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

let isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
let buffer_size = 256;
let audio_input = null;
let code = "import(\"stdfaust.lib\"); process = sum(i, 1, os.oscsin(440.0));"
let argv = Array();
let libraries_url = "http://0.0.0.0:8000/lib/faust/libraries/";
argv.push("-ftz");
argv.push("2");
argv.push("-I");
argv.push(libraries_url);

// Create a DSP factory from the dsp code
faust_module['onRuntimeInitialized'] = startDSP;

function startDSP() {
    faust.createDSPFactory(code, argv, (factory) => {
        if (!factory) {
            alert(faust.getErrorMessage());
            return;
        }
        
        if (workletAvailable()) {
            faust.createDSPInstance(factory, audio_context, buffer_size, (worklet) => {
                worklet.connect(audio_context.destination);
            });
        } else {
            // Then creates one DSP instance
            faust.createDSPInstance(factory, audio_context, buffer_size, (node) => {
                node.connect(audio_context.destination);
            });
        }
    });
};

// -- borrowed from Google's AudioWorklet demo page
function workletAvailable()
{
    return window.audioWorklet &&
    typeof window.audioWorklet.addModule === 'function' &&
    window.AudioWorkletNode;
}
/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

AFRAME.registerComponent('raycaster-listen', {
  init: function () {
    this.speed = undefined;
    this.previous_intersection_point = undefined;
    this.current_intersection_point = undefined;
    this.previous_timestamp = undefined;
    this.current_timestamp = undefined;

    // Use events to figure out what raycaster is listening so we don't have to
    // hardcode the raycaster.
    this.el.addEventListener('raycaster-intersected', evt => {
      this.raycaster = evt.detail.el;
    });
    this.el.addEventListener('raycaster-intersected-cleared', evt => {
      this.raycaster = null;
      this.current_intersection_point = undefined;
    });
  },

  tick: function () {
    if (this.raycaster) {
      let intersection = this.raycaster.components.raycaster.getIntersection(this.el);
      if (intersection) {
        this.current_intersection_point = intersection.point;
        this.current_timestamp = performance.now();
      }
    }

    if (this.previous_intersection_point && this.current_intersection_point) {
      let displacement = this.previous_intersection_point.distanceTo(this.current_intersection_point);
      let duration = ((this.current_timestamp - this.previous_timestamp) / 1000);
      this.speed = displacement/duration;
      console.log(this.speed);
    }

    this.previous_intersection_point = this.current_intersection_point;
    this.previous_timestamp = this.current_timestamp;
  }
});
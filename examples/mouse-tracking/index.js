/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

document.addEventListener('DOMContentLoaded', function () {
    document.addEventListener('mousemove', mouseMoveCallback);
})

function mouseMoveCallback(e)
{
    console.log(e.screenX, e.screenY);
}
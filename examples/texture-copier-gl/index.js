/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

AFRAME.registerComponent('raycast-listen', {
  init: function () {
    
    // Use events to figure out what raycaster is listening so we don't have to
    // hardcode the raycaster.
    this.el.addEventListener('raycaster-intersected', evt => {
      this.raycaster = evt.detail.el;
    });
    this.el.addEventListener('raycaster-intersected-cleared', evt => {
      this.raycaster = null;
    });
  },
  
  tick: function () {
    if (!this.raycaster) { return; }  // Not intersecting.

    let intersection = this.raycaster.components.raycaster.getIntersection(this.el);
    if (!intersection) { return; }
    this.el.emit('felt', intersection);
  }
});

AFRAME.registerComponent('feelable', {
  init: function () {
    this.canvas = document.createElement('canvas');
    this.gl = this.canvas.getContext('webgl');
    this.texture = this.gl.createTexture();
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
    this.framebuffer = this.gl.createFramebuffer();
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.framebuffer);

    // Wait for model to load.
    this.el.addEventListener('model-loaded', () => {
      // Grab the mesh / scene.
      const obj = this.el.getObject3D('mesh');
    });
    
    this.el.addEventListener('felt', (event) => {
      let intersection = event.detail;
      
      let roughness = (() => {
        let roughnessMap = this.getRoughnessMap(intersection.object);
        if (roughnessMap) {
          let [x,y] = [Math.round(intersection.uv.x * roughnessMap.width), Math.round(intersection.uv.y * roughnessMap.height)];
          return this.getImageData(roughnessMap, x, y);
        } else {
          return undefined;
        };
      })();
      
      let displacement = (() => {
        let displacementMap = this.getDisplacementMap(intersection.object);
        if (displacementMap) {
          let [x,y] = [Math.round(intersection.uv.x * displacementData.width), Math.round(intersection.uv.y * displacementData.height)];
          return this.getImageData(displacementMap, x, y);
        } else {
          return undefined;
        };
      })();

      console.log(roughness, displacement);
    });
  },

  remove: function() {
    this.gl.deleteTexture(this.texture);
    this.gl.deleteFramebuffer(this.framebuffer);
    this.gl = null;
    this.canvas = null;
  },

  getRoughnessMap: function(obj) {
    return _.get(obj, ['material', 'roughnessMap', 'image']);
  },

  getDisplacementMap: function(obj) {
    return _.get(obj, ['material', 'displacementMap', 'image']);
  },

  getImageData: function(image, x, y) {
    [canvas, gl, texture, framebuffer] = [this.canvas, this.gl, this.texture, this.framebuffer];

    this.canvas.width = image.width;
    this.canvas.height = image.height;

    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

    var pixels = new Uint8Array(4);
    gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixels);

    return pixels[3];
  }
});
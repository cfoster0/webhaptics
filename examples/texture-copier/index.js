/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

AFRAME.registerComponent('raycast-listen', {
  init: function () {
    
    // Use events to figure out what raycaster is listening so we don't have to
    // hardcode the raycaster.
    this.el.addEventListener('raycaster-intersected', evt => {
      this.raycaster = evt.detail.el;
    });
    this.el.addEventListener('raycaster-intersected-cleared', evt => {
      this.raycaster = null;
    });
  },
  
  tick: function () {
    if (!this.raycaster) { return; }  // Not intersecting.

    let intersection = this.raycaster.components.raycaster.getIntersection(this.el);
    if (!intersection) { return; }
    this.el.emit('felt', intersection);
  }
});

AFRAME.registerComponent('feelable', {
  init: function () {
    // Wait for model to load.
    this.el.addEventListener('model-loaded', () => {
      // Grab the mesh / scene.
      const obj = this.el.getObject3D('mesh');
      // Create a set of cached references to the textures
      this.cache = {};
    });
    
    this.el.addEventListener('felt', (event) => {
      let intersection = event.detail;
      
      let roughness = (() => {
        let roughnessMap = this.getRoughnessMap(intersection.object);
        if (!roughnessMap) {
          return;
        };
        let roughnessData = this.getImageData(roughnessMap);
        let [x,y] = [Math.round(intersection.uv.x * roughnessData.width), Math.round((1.0 - intersection.uv.y) * roughnessData.height)];
        let index = 4 * (y * roughnessData.width + x) + 3;
        return roughnessData.data[index];
      })();
      
      let displacement = (() => {
        let displacementMap = this.getDisplacementMap(intersection.object);
        if (!displacementMap) {
          return;
        };
        let displacementData = this.getImageData(roughnessMap);
        let [x,y] = [Math.round(intersection.uv.x * displacementData.width), Math.round(intersection.uv.y * displacementData.height)];
        let index = 4 * (y * displacementData.width + x) + 3;
        return displacementData.data[index];
      })();

      console.log(roughness, displacement);
    });
  },

  getRoughnessMap: function(obj) {
    return _.get(obj, ['material', 'roughnessMap', 'image']);
  },

  getDisplacementMap: function(obj) {
    return _.get(obj, ['material', 'displacementMap', 'image']);
  },

  getImageData: function(image) {
    if (image in this.cache) {
      return this.cache[image];
    } else {
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
  
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0);

      imgData = context.getImageData(0, 0, image.width, image.height);

      this.cache[image] = imgData;
  
      return imgData;
    };
  }
});
/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

AFRAME.registerComponent('modify-materials', {
  init: function () {
    // Wait for model to load.
    this.el.addEventListener('model-loaded', () => {
      // Grab the mesh / scene.
      const obj = this.el.getObject3D('mesh');
      // Go over the submeshes and modify materials we want.
      obj.traverse(node => {
        if (node.material) {
          if (node.material.roughnessMap) {
            console.log("Found material roughness map");
            var c = document.createElement('canvas');
            var ctx = c.getContext("2d");
            var img = new Image();
            img.src = node.material.roughnessMap.image.src;
            img.crossOrigin = "anonymous";
            c.append(img);
            this.el.append(c);
            img.onload = (() => {
              c.height = img.height;
              c.width = img.width;
              ctx.drawImage(img, 0, 0);
              var imgData = ctx.getImageData(0, 0, c.width, c.height);
  
              var x = 0.5;
              var y = 0.25;

              var i = (Math.round(y * imgData.width) + Math.round(x * imgData.height)) * 4;
              console.log(i);
              console.log(imgData.data[i], imgData.data[i + 1], imgData.data[i + 2], imgData.data[i + 3]);
            });
          }
        }
      });
    });
  }
});
/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

let isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
let buffer_size = 512;

const convertCodeToModel = function(code) {
    let libraries_url = "http://0.0.0.0:8000/lib/faust/libraries/";
    let argv = Array("-ftz", "2", "-I", libraries_url);

    // Create a DSP factory from the dsp code
    let factoryPromise = new Promise((resolve, reject) => {
        faust_module.then(() => {
            faust.createDSPFactory(code, argv, (factory) => {
                if (!factory) {
                    reject(faust.getErrorMessage());
                }
                resolve(factory);
            });
        });
    });
    
    let dspNode = new Promise((resolve, reject) => {
        factoryPromise.then((factory) => {
            if (workletAvailable()) {
            faust.createDSPWorkletInstance(factory, audio_context, resolve);
            } else {
            faust.createDSPInstance(factory, audio_context, buffer_size, resolve);
            }
        });
    });

    return dspNode;
}

document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
});

const handleFileSelect = ((evt) => {
    let files = Array.from(
                    evt
                    .target
                    .files
                );
    let f = files.slice(-1)[0];

    let reader = new FileReader();
     
    reader.onload = (function(theFile) {
        return function(e) {
            let image = new Image();
            image.onload = () => {
                let DFTPromise = x2fft.image2fft_limited(image, "intensity", 100);
                DFTPromise.then((DFTResults) => {
                    let code = fftToCode(DFTResults);
                    let node = convertCodeToModel(code);
                    node.then((model) => {
                        model.connect(audio_context.destination);
                        model.setParamValue("/FaustDSP/gain", 1000.0);
                    });
                    image.addEventListener('mousemove', (ev) => {
                        let borders = image.getBoundingClientRect();
                        let x = (ev.clientX - borders.left) / borders.width;
                        let y = (ev.clientY - borders.top) / borders.height;
                        node.then((model) => {
                            model.setParamValue("/FaustDSP/x", x);
                            model.setParamValue("/FaustDSP/y", y);
                        });
                    });
                });
            };
            image.src = e.target.result;
            document.getElementById('display').appendChild(image);
        };
    })(f);
    
    reader.readAsDataURL(f);
});

function fftToCode(DFTResults) {
    let selected_indices = DFTResults.indices;
    let selected_reals = DFTResults.real;
    let selected_imaginaries = DFTResults.imaginary;
    let selected_amplitudes = _.zip(selected_reals, selected_imaginaries);

    let amplitude_string = `${
                            selected_amplitudes
                                .map((value) => {
                                    return "(" + value[0] + "," + value[1] + ")";
                                })
                                .join()
                            }`;
    let index_string = `${
                        selected_indices
                            .map((value) => {
                                return "(" + value[0] + "," + value[1] + ")";
                            })
                            .join()
                        }`;

    let code = `import("stdfaust.lib");
    M = ${DFTResults.dimensions[0]};
    N = ${DFTResults.dimensions[1]};
    T = 1 / ma.SR;
    magnitude(a, b) = sqrt(a*a + b*b);
    amplitudes(n) = ba.take(n+1, (${amplitude_string}, (0,0)));
    indices(n) = ba.take(n+1, (${index_string}, (0,0)));
    spatialMap(x, y, z) = max(heightEnvelope - z, 0) * roughnessNoise * velocity with {
        n_components = ${selected_indices.length};
        heightEnvelope = ((components + 1) / 2);
        roughnessNoise = no.pink_noise;
        components = sum(i, n_components, spatialEnvelope(i)) with {
        spatialEnvelope(n) = (1 / n_components) + ((wm(n), wn(n) : si.cmul), amplitudes(n) : si.cmul : _, si.block(1));
        wm(n) = cos(j(n) * y * 2 * ma.PI / M), sin(j(n) * y * 2 * ma.PI / M);
        wn(n) = cos(k(n) * x * 2 * ma.PI / N), sin(k(n) * x * 2 * ma.PI / N);
        j(n) = indices(n) : _, si.block(1);
        k(n) = indices(n) : si.block(1), _;
        };
        velocity = magnitude((x - x@1), (y - y@1)) : si.smooth(ba.tau2pole(0.028));
    };
    xslider = hslider("x", 0, 0, 1, 0.001) : _ * N : si.smooth(ba.tau2pole(0.028));
    yslider = vslider("y", 0, 0, 1, 0.001) : _ * M : si.smooth(ba.tau2pole(0.028));
    heightslider = nentry("height", 1.15, 0.0, 1.5, 0.01);
    gain = nentry("gain", 1, 0, 1000, 0.01);
    process = spatialMap(xslider, yslider, heightslider) * gain;`;
    document.getElementById('code').innerHTML = `<code>${code}</code>`
                                                    .replace(/;/g, ";<br>")
                                                    .replace(/{/g, "{<br>");
    return code;
}

// -- borrowed from Google's AudioWorklet demo page
function workletAvailable()
{
    return window.audioWorklet &&
    typeof window.audioWorklet.addModule === 'function' &&
    window.AudioWorkletNode;
}
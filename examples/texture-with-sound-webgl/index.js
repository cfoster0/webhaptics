/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

let isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
let audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
let buffer_size = 256;
let audio_input = null;
let code = "import(\"stdfaust.lib\"); roughness = nentry(\"roughness\", 0.0, 0.0, 1.0, 0.001); speed = nentry(\"speed\", 0.0, 0.0, 100.0, 0.01); process = roughness * no.pink_noise * speed;";
let argv = Array();
let libraries_url = "http://0.0.0.0:8000/lib/faust/libraries/";
argv.push("-ftz");
argv.push("2");
argv.push("-I");
argv.push(libraries_url);

let scrapeSpeed = 0;

// Create a DSP factory from the dsp code
const factoryPromise = new Promise((resolve, reject) => {
  faust_module.then(() => {
    faust.createDSPFactory(code, argv, (factory) => {
      if (!factory) {
          reject(faust.getErrorMessage());
      }
      resolve(factory);
    });
  })
});

AFRAME.registerComponent('feelable', {
  dependencies: ['gltf-model'],

  init: function () {
    this.canvas = document.createElement('canvas');
    this.gl = this.canvas.getContext('webgl');
    this.texture = this.gl.createTexture();
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
    this.framebuffer = this.gl.createFramebuffer();
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.framebuffer);

    this.contact_model = new Promise((resolve, reject) => {
      factoryPromise.then((factory) => {
        if (workletAvailable()) {
          faust.createDSPWorkletInstance(factory, audio_context, resolve);
        } else {
          faust.createDSPInstance(factory, audio_context, buffer_size, resolve);
        }
      });
    });

    this.contact_model.then((model) => {
      model.connect(audio_context.destination);
    });

    // Wait for model to load.
    this.el.addEventListener('model-loaded', () => {
      // Grab the mesh / scene.
      const obj = this.el.getObject3D('mesh');
    });
    
    this.el.addEventListener('felt', (event) => {
      let intersection = event.detail;
      
      let roughness = (() => {
        let roughnessMap = this.getRoughnessMap(intersection.object);
        if (roughnessMap) {
          let [x,y] = [Math.round(intersection.uv.x * roughnessMap.width), Math.round(intersection.uv.y * roughnessMap.height)];
          return this.getImageData(roughnessMap, x, y);
        } else {
          return undefined;
        };
      })();
      
      let displacement = (() => {
        let displacementMap = this.getDisplacementMap(intersection.object);
        if (displacementMap) {
          let [x,y] = [Math.round(intersection.uv.x * displacementData.width), Math.round(intersection.uv.y * displacementData.height)];
          return this.getImageData(displacementMap, x, y);
        } else {
          return undefined;
        };
      })();

      
      this.contact_model.then((model) => {
        console.log(roughness, scrapeSpeed);
        model.setParamValue("/FaustDSP/roughness", roughness);
        model.setParamValue("/FaustDSP/speed", scrapeSpeed);
      });
    });
  },

  remove: function() {
    this.gl.deleteTexture(this.texture);
    this.gl.deleteFramebuffer(this.framebuffer);
    this.gl = null;
    this.canvas = null;
  },

  getRoughnessMap: function(obj) {
    return _.get(obj, ['material', 'roughnessMap', 'image']);
  },

  getDisplacementMap: function(obj) {
    return _.get(obj, ['material', 'displacementMap', 'image']);
  },

  getImageData: function(image, x, y) {
    let [canvas, gl, texture, framebuffer] = [this.canvas, this.gl, this.texture, this.framebuffer];

    canvas.width = image.width;
    canvas.height = image.height;

    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

    var pixels = new Uint8Array(4);
    gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixels);

    //return pixels[3] / 255.0;
    return pixels[0] / 255.0;
  }
});

AFRAME.registerComponent('moveable', {
  init: function() {
    this.speed = 0;
    this.previous = undefined;
    this.current = undefined;
    this.previous_timestamp = undefined;
    this.current_timestamp = undefined;
  },

  tick: function() {
    let position = this.el.object3D.position;
    this.current = new AFRAME.THREE.Vector3(position.x, position.y, position.z);
    this.current_timestamp = performance.now();

    if (this.previous && this.current) {
      let displacement = this.previous.distanceTo(this.current);
      let duration = ((this.current_timestamp - this.previous_timestamp) / 1000);
      if (displacement/duration !== this.speed) {
        this.speed = displacement/duration;
        this.el.emit('moved', {old: this.previous, new: this.current, speed: this.speed});
        scrapeSpeed = this.speed;
      }
    }
    this.previous = this.current;
    this.previous_timestamp = this.current_timestamp;    
  }
});

AFRAME.registerComponent('raycast-listen', {
  init: function() {
    // Use events to figure out what raycaster is listening so we don't have to
    // hardcode the raycaster.
    this.el.addEventListener('raycaster-intersected', evt => {
      this.raycaster = evt.detail.el;
    });
    this.el.addEventListener('raycaster-intersected-cleared', evt => {
      this.raycaster = null;
    });
  },

  tick: function () {
    if (!this.raycaster) { return; }  // Not intersecting.

    let intersection = this.raycaster.components.raycaster.getIntersection(this.el);
    if (!intersection) { return; }
    this.el.emit('felt', intersection);
  }
});

AFRAME.registerComponent('raycast-reticle', {
  dependencies: ['moveable'],

  init: function() {
    this.raycaster = this.el.parentEl;
    this.intersected = undefined;

    this.raycaster.addEventListener('raycaster-intersection', evt => {
      this.intersected = evt.detail.els[0];
    });

    this.raycaster.addEventListener('raycaster-intersection-cleared', evt => {
      this.intersected = undefined;
    });
  },

  tick: function() {
    if (this.intersected) {
      let intersection = this.raycaster.components.raycaster.getIntersection(this.intersected);
      this.el.object3D.position.set(intersection.point.x, intersection.point.y, intersection.point.z);
    }
  }
});

// -- borrowed from Google's AudioWorklet demo page
function workletAvailable()
{
    return window.audioWorklet &&
    typeof window.audioWorklet.addModule === 'function' &&
    window.AudioWorkletNode;
}
/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

"use strict";

mario_vibrate();

function tiny_vibrate()
{
    navigator.vibrate([1, 15, 1, 15, 1, 15, 1, 15, 1, 15, 1, 15, 1, 15, 1, 15, 1, 15, 1]);
    console.log("Vibrated");
}

function increasing_vibrate()
{
    navigator.vibrate([1, 500, 2, 500, 4, 500, 8, 500, 16, 500, 32, 500, 64, 500, 128, 500, 256, 500, 512, 500, 1024]);
    console.log("Vibrated");
}

function mario_vibrate()
{
    navigator.vibrate([50,30,50,110,80,110,50,30,50,110,80,240,80,240]);
    //navigator.vibrate([125,75,125,275,200,275,125,75,125,275,200,600,200,600]);
    console.log("Vibrated");
}
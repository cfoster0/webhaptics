/**
* Copyright 2019 by Charles Foster <cfoster0@ccrma.stanford.edu>
* 
* This file is part of WebHaptics.
* 
* WebHaptics is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* WebHaptics is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with WebHaptics.  If not, see <https://www.gnu.org/licenses/>.
*/

AFRAME.registerComponent('raycast-roughness-listen', {
  init: function () {
    // Wait for model to load.
    this.el.addEventListener('model-loaded', () => {
      // Grab the mesh / scene.
      const obj = this.el.getObject3D('mesh');
      // Go over the submeshes and modify materials we want.
      obj.traverse(node => {
        if (node.material) {
          if (node.material.roughnessMap) {
            console.log("Found material roughness map");
            var c = document.createElement('canvas');
            var ctx = c.getContext("2d");
            var img = new Image();
            img.src = node.material.roughnessMap.image.src;
            img.crossOrigin = "anonymous";
            c.append(img);
            this.el.append(c);
            img.onload = (() => {
              c.height = img.height;
              c.width = img.width;
              ctx.drawImage(img, 0, 0);
              this.roughnessMap = ctx.getImageData(0, 0, c.width, c.height);
            });
          }
        }
      });

    });
    
    // Use events to figure out what raycaster is listening so we don't have to
    // hardcode the raycaster.
    this.el.addEventListener('raycaster-intersected', evt => {
      this.raycaster = evt.detail.el;
    });
    this.el.addEventListener('raycaster-intersected-cleared', evt => {
      this.raycaster = null;
    });
  },

  getRoughness: function(x, y) {
    var i = Math.round(y * this.roughnessMap.width) + (Math.round(x * this.roughnessMap.height) * 4);
    return this.roughnessMap.data[i + 3];
  },
  
  tick: function () {
    if (!this.raycaster) { return; }  // Not intersecting.

    let intersection = this.raycaster.components.raycaster.getIntersection(this.el);
    if (!intersection) { return; }
    console.log(this.getRoughness(intersection.uv.x, intersection.uv.y));
  }
});
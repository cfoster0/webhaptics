const ndarray = require('ndarray');
const pool = require('ndarray-scratch');
const fft = require('ndarray-fft');
const image_pixels = require('ndarray-from-image');
const url_pixels = require('get-pixels');
const { GPU } = require('gpu.js');
//const canvas = require('ndarray-canvas');

const math = require('mathjs');

const gpu = new GPU();

module.exports = {
    url2fft: function url2fft(src, mode) {
        return new Promise((resolve, reject) => {
            url_pixels(src, function (err, data) {
                if (err) reject(err);

                let re = ndarray(new Float32Array(data.shape[0] * data.shape[1]), [data.shape[0], data.shape[1]]);

                if (mode === "alpha") {
                    for (let i = 0; i < data.shape[0]; i++) {
                        for (let j = 0; j < data.shape[1]; j++) {
                            re.set(i, j, data.get(i, j, 3) / 255.0);
                        }
                    }
                } else if (mode === "intensity") {
                    for (let i = 0; i < data.shape[0]; i++) {
                        for (let j = 0; j < data.shape[1]; j++) {
                            let intensity = (data.get(i, j, 0) + data.get(i, j, 1) + data.get(i, j, 2)) / 3.0;
                            re.set(j, i, intensity);
                        }
                    }
                }

                let im = pool.zeros(re.shape, "float32");
                let width = re.shape[0];
                let height = re.shape[1];
                fft(1, re, im);
                re = pool.clone(re);
                im = pool.clone(im);    

                let kernel = gpu.createKernel(function(a, b) {
                    const real = a[this.thread.x] * a[this.thread.x];
                    const imag = b[this.thread.x] * b[this.thread.x];
                    return Math.sqrt(real + imag);
                }).setOutput([height * width]);

                let mag = kernel(re.data, im.data);

                resolve({real: re.data, imaginary: im.data, magnitude: mag, dimensions: [height, width]});
            });
        });
    },

    image2fft: function image2fft(image, mode) {
        return new Promise((resolve, reject) => {
            let data = image_pixels(image);

            let re = ndarray(new Float32Array(data.shape[0] * data.shape[1]), [data.shape[0], data.shape[1]]);

            if (mode === "alpha") {
                for (let i = 0; i < data.shape[0]; i++) {
                    for (let j = 0; j < data.shape[1]; j++) {
                        re.set(i, j, data.get(i, j, 3) / 255.0);
                    }
                }
            } else if (mode === "intensity") {
                for (let i = 0; i < data.shape[0]; i++) {
                    for (let j = 0; j < data.shape[1]; j++) {
                        //let intensity = (data.get(i, j, 0) + data.get(i, j, 1) + data.get(i, j, 2) + data.get(i, j, 3)) / 4.0 / 255.0;
                        let intensity = (data.get(i, j, 0) + data.get(i, j, 1) + data.get(i, j, 2)) / 3.0;
                        re.set(j, i, intensity);
                    }
                }
            }

            let im = pool.zeros(re.shape, "float32");
            let width = re.shape[0];
            let height = re.shape[1];
            fft(1, re, im);
            re = pool.clone(re);
            im = pool.clone(im);

            let kernel = gpu.createKernel(function(a, b) {
                const real = a[this.thread.x] * a[this.thread.x];
                const imag = b[this.thread.x] * b[this.thread.x];
                return Math.sqrt(real + imag);
            }).setOutput([height * width]);

            /*
            let can = canvas(null, re);
            document.body.appendChild(can);
            */

            let mag = kernel(re.data, im.data);

            resolve({real: re.data, imaginary: im.data, magnitude: mag, dimensions: [height, width]});
        });
    },

    image2fft_limited: function image2fft_limited(image, mode, n_components) {
        return new Promise((resolve, reject) => {
            let data = image_pixels(image);

            let re = ndarray(new Float32Array(data.shape[0] * data.shape[1]), [data.shape[0], data.shape[1]]);

            if (mode === "alpha") {
                for (let i = 0; i < data.shape[0]; i++) {
                    for (let j = 0; j < data.shape[1]; j++) {
                        re.set(i, j, data.get(i, j, 3) / 255.0);
                    }
                }
            } else if (mode === "intensity") {
                for (let i = 0; i < data.shape[0]; i++) {
                    for (let j = 0; j < data.shape[1]; j++) {
                        //let intensity = (data.get(i, j, 0) + data.get(i, j, 1) + data.get(i, j, 2) + data.get(i, j, 3)) / 4.0 / 255.0;
                        let intensity = (data.get(i, j, 0) + data.get(i, j, 1) + data.get(i, j, 2)) / 3.0;
                        re.set(j, i, intensity);
                    }
                }
            }

            let im = pool.zeros(re.shape, "float32");
            let width = re.shape[0];
            let height = re.shape[1];
            fft(1, re, im);
            let dft_re = pool.clone(re);
            let dft_im = pool.clone(im);

            let kernel = gpu.createKernel(function(a, b) {
                const real = a[this.thread.x] * a[this.thread.x];
                const imag = b[this.thread.x] * b[this.thread.x];
                return Math.sqrt(real + imag);
            }).setOutput([height * width]);

            let mag = kernel(dft_re.data, dft_im.data);

            let DFTResults = {real: dft_re.data, imaginary: dft_im.data, magnitude: mag, dimensions: [height, width]};


            let angles = _.zip(DFTResults.real, DFTResults.imaginary)
                          .map((complex) => {
                              return math.atan(complex[1]/complex[0]);
                          });

            let magnitudes = DFTResults.magnitude;

            let maxes = new Array();
            let max_indices = new Array();

            for (let i = 0; i < n_components; i++) {
                // https://stackoverflow.com/questions/11301438/return-index-of-greatest-value-in-an-array/11301464
                let [max, max_index] = magnitudes.reduce((a,b,c) => a[0] < b ? [b,c] : a, [Number.MIN_VALUE,-1]);
                maxes.push(max);
                max_indices.push(max_index);
                magnitudes[max_index] = 0;
                let conjugate = angles[max_index] * -1;
                // https://stackoverflow.com/questions/20798477/how-to-find-index-of-all-occurrences-of-element-in-array
                let duplicates = angles.reduce((a, e, c) => (e === conjugate) ? a.concat(c) : a, []);
                duplicates.map((index) => {
                    magnitudes[index] = 0;
                });
            }
       
            let normalization_constant = maxes
                                    .reduce((accumulator, value) => {
                                        return accumulator + value;
                                    });        

            let selected_indices = max_indices
                                    .map((index) => {
                                        return new Array(index % DFTResults.dimensions[1], Math.floor(index / DFTResults.dimensions[1]));
                                    });
            let selected_reals = max_indices
                                    .map((index) => {
                                        return DFTResults.real[index] / normalization_constant;
                                    });
            let selected_imaginaries = max_indices
                                        .map((index) => {
                                            return DFTResults.imaginary[index] / normalization_constant;
                                        });

            let reduced_re = pool.zeros(re.shape, "float32");
            let reduced_im = pool.zeros(im.shape, "float32");
            selected_indices.map((index_pair) => {
                reduced_re.set(index_pair[0], index_pair[1], re.get(index_pair[0], index_pair[1]));
                reduced_im.set(index_pair[0], index_pair[1], im.get(index_pair[0], index_pair[1]));
            });

            /*
            fft(-1, reduced_re, reduced_im);

            let inverse_re = pool.clone(reduced_re);
            let inverse_im = pool.clone(reduced_im);

            
            let can = canvas(null, inverse_re);
            document.body.appendChild(can);
            */

            resolve({real: selected_reals, imaginary: selected_imaginaries, indices: selected_indices, dimensions: [height, width]});
        });
    }
}

/*
let url2fft = module.exports.url2fft;
let image2fft = module.exports.image2fft;
url2fft("https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/BarramundiFish/glTF/BarramundiFish_occlusionRoughnessMetallic.png", "intensity");
let image = new Image();
image.src = "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/BarramundiFish/glTF/BarramundiFish_occlusionRoughnessMetallic.png";
image.onload = () => {
    image2fft(image);
}
*/